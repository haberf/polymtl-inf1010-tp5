#ifndef EXCEPTIONJEUPASDANSLALIBRAIRIE_H
#define EXCEPTIONJEUPASDANSLALIBRAIRIE_H

#include <QException>
#include <QString>

class ExceptionJeuPasDansLaLibrairie : public QException
{
public:
    ExceptionJeuPasDansLaLibrairie(QString s) :
		s_(s) {};
    QString *  what()  { return &s_; }
private:
	QString s_;
};

#endif // EXCEPTIONJEUPASDANSLALIBRAIRIE_H
