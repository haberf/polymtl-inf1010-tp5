#ifndef EXCEPTIONJEUDEJAACHETER_H
#define EXCEPTIONJEUDEJAACHETER_H

#include <QException>
#include <QString>

class ExceptionJeuDejaAcheter : public QException
{
public:
	ExceptionJeuDejaAcheter(QString s) :
		s_(s) {};
   QString*  what()  { return &s_; };
private:
	QString s_;
};


#endif // EXCEPTIONJEUDEJAACHETER_H
