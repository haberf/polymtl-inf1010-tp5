#ifndef EXCEPTIONFONDSINSUFFISANTS_H
#define EXCEPTIONFONDSINSUFFISANTS_H

#include <QException>
#include <QString>

class ExceptionFondsInsuffisants : public QException
{
public:
    ExceptionFondsInsuffisants(QString s) :
		s_(s) {};
    QString* what() { return &s_; }
private:
	QString s_;
};

#endif // EXCEPTIONFONDSINSUFFISANTS_H
