/// Gestionnaire d'utilisateurs.

/// \author Misha Krieger-Raynauld

/// \date 2020-10-29



#include "GestionnaireUtilisateurs.h"

#include <fstream>

#include <iomanip>

#include <iostream>

#include <sstream>



/// Affiche les informations des utilisateurs gérés par le gestionnaire d'utilisateurs à la sortie du stream donné.

/// \param outputStream             Le stream auquel écrire les informations des utilisateurs.

/// \param gestionnaireUtilisateurs Le gestionnaire d'utilisateurs à afficher au stream.

/// \return                         Une référence au stream.

std::ostream& operator<<(std::ostream& outputStream, const GestionnaireUtilisateurs& gestionnaireUtilisateurs)

{



    outputStream << "Le gestionnaire d'utilisateurs contient " << gestionnaireUtilisateurs.getNombreUtilisateurs()

        << " utilisateurs:\n";





    for (const auto& [first, second] :

        gestionnaireUtilisateurs.utilisateurs_)

    {



        outputStream << '\t' << second << '\n';



    }



    return outputStream;



}

/// Ajoute les utilisateurs à partir d'un fichier de données d'utilisateurs.

/// \param nomFichier   Le fichier à partir duquel lire les informations des utilisateurs.

/// \return             True si tout le chargement s'est effectué avec succès, false sinon.

bool GestionnaireUtilisateurs::chargerDepuisFichier(const std::string& nomFichier)

{

    std::ifstream fichier(nomFichier);

    if (!fichier)

    {

        std::cerr << "Erreur GestionnaireUtilisateurs: le fichier " << nomFichier << " n'a pas pu être ouvert\n";

        return false;

    }



    utilisateurs_.clear();



    bool succesParsing = true;



    std::string ligne;

    while (std::getline(fichier, ligne))

    {

        std::istringstream stream(ligne);



        std::string id;

        std::string nom;

        int age;

        int pays;



        if (stream >> id >> std::quoted(nom) >> age >> pays)

        {



            ajouterUtilisateur(Utilisateur{ id, nom, age, static_cast<Pays>(pays) });

        }

        else

        {

            std::cerr << "Erreur GestionnaireUtilisateurs: la ligne " << ligne

                << " n'a pas pu être interprétée correctement\n";

            succesParsing = false;

        }

    }

    return succesParsing;

}



bool  GestionnaireUtilisateurs::ajouterUtilisateur(const Utilisateur& utilisateur)

{

    return std::get<1>(utilisateurs_.emplace(std::make_pair(utilisateur.id, utilisateur)));

}



bool  GestionnaireUtilisateurs::supprimerUtilisateur(const std::string& idUtilisateur)

{

    return utilisateurs_.erase(idUtilisateur);

}



std::size_t GestionnaireUtilisateurs::getNombreUtilisateurs() const

{

    return utilisateurs_.size();

}



const Utilisateur* GestionnaireUtilisateurs::getUtilisateurParId(const std::string& id) const

{

    auto utilisateur = utilisateurs_.find(id);

    if (utilisateur == utilisateurs_.end())

    {

        return nullptr;

    };

    return &utilisateur->second;



}