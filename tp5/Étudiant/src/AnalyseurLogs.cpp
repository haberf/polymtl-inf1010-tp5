/// Analyseur de statistiques grâce aux logs.
/// \author Misha Krieger-Raynauld
/// \date 2020-10-29

#include "AnalyseurLogs.h"
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <unordered_set>
#include "Foncteurs.h"

/// Ajoute les lignes de log en ordre chronologique à partir d'un fichier de logs.
/// \param nomFichier               Le fichier à partir duquel lire les logs.
/// \param gestionnaireUtilisateurs Référence au gestionnaire des utilisateurs pour lier un utilisateur à un log.
/// \param gestionnaireJeux         Référence au gestionnaire des jeux pour pour lier un jeu à un log.
/// \return                         True si tout le chargement s'est effectué avec succès, false sinon.
bool AnalyseurLogs::chargerDepuisFichier(const std::string& nomFichier,
                                         GestionnaireUtilisateurs& gestionnaireUtilisateurs,
                                         GestionnaireJeux& gestionnaireJeux)
{
    std::ifstream fichier(nomFichier);
    if (!fichier)
    {
        std::cerr << "Erreur AnalyseurLogs: le fichier " << nomFichier << " n'a pas pu être ouvert\n";
        return false;
    }

    logs_.clear();
    heuresJeux_.clear();

    bool succesParsing = true;

    std::string ligne;
    while (std::getline(fichier, ligne))
    {
        std::istringstream stream(ligne);

        std::string timestamp;
        std::string idUtilisateur;
        std::string nomJeu;
        int heures;

        if (stream >> timestamp >> idUtilisateur >> std::quoted(nomJeu) >> heures)
        {
            creerLigneLog lignelog(timestamp, idUtilisateur, nomJeu, heures, gestionnaireUtilisateurs, gestionnaireJeux);
            ajouterLigneLogNonTriee(lignelog);
        }
        else
        {
            std::cerr << "Erreur AnalyseurLogs: la ligne " << ligne << " n'a pas pu être interprétée correctement\n";
            succesParsing = false;
        }
    }
    trierLignesLog();
    return succesParsing;
}
LigneLog AnalyseurLogs::creerLigneLog(const std::string& timestamp, const std::string& idUtilisateur, const std::string& nomJeu, int heures, GestionnaireUtilisateurs& gestionnaireUtilisateurs, GestionnaireJeux& gestionnaireJeux)
{
    const Utilisateur utilisateur = gestionnaireUtilisateurs.getUtilisateurParId(idUtilisateur);
    const Jeu jeu = gestionnaireJeux.getJeuParNom(nomJeu);
    return LigneLog ligneLog{ timestamp, utilisateur, jeu ,heures };
}
bool  AnalyseurLogs::ajouterLigneLogNonTriee(const LigneLog& ligneLog) 
{
    if (ligneLog.utilisateur == nullptr || lignelog.jeu == nullptr) { return false; }
    logs_.push_back(ligneLog);
    heuresJeux_[ligneLog.jeu] += ligneLog.heures;
    return true;
}
void AnalyseurLogs::trierLignesLog()
{
    std::sort(logs_.begin(), logs_.end(), ComparateurLog());
}
bool AnalyseurLogs::ajouterLigneLog(const std::string& timestamp, const
    std::string& idUtilisateur, const std::string& nomJeu, int heures,
    GestionnaireUtilisateurs& gestionnaireUtilisateurs,
    GestionnaireJeux& gestionnaireJeux)
{   
    LigneLog lignelog = creerLigneLog(const std::string & timestamp, const
        std::string & idUtilisateur, const std::string & nomJeu, int heures,
        GestionnaireUtilisateurs & gestionnaireUtilisateurs,
        GestionnaireJeux & gestionnaireJeux);
    return ajouterLigneLog(lignelog);
}
bool ajouterLigneLog(const LigneLog& ligneLog) 
{
    auto lower = std::lower_bound(logs_.begin(), logs_.end(), ligneLog.timestamp);

}
int getHeuresJoueesPourJeu(const Jeu* jeu) const
    logs_.insert(lower, ligneLog);
    heuresJeux_[ligneLog.jeu] += ligneLog.heures;
    return true;
}
int AnalyseurLogs::getHeuresJoueesPourJeu(const Jeu* jeu) const
{
    if (heuresJeux_.find(jeu) == heuresJeux_.end())
    {
        return 0;
    }
    return heuresJeux_.at(jeu);
}

std::vector<std::pair<const Jeu*, int>> getNJeuxPlusPopulaires(std::size_t nombre) const
{
    std::vector<std::pair<const Jeu*, int>> nJeuxPlusPopulaires(std::min(heuresJeux_.end(),nJeuxPlusPopulaires.begin(), nJeuxPlusPopulaires.end(),nombre));
    std::partial_sort_copy(heuresJeux_.begin(), heuresJeux_.end(), nJeuxPlusPopulaires.begin(), nJeuxPlusPopulaires.end(),[](const std::pair<const Jeu*, int>& jeu1,const std::pair<const Jeu*, int>& jeu1)
    {
        return jeu1.second > jeu2.second;
    }
    return nJeuxPlusPopulaires;
}



const Jeu* AnalyseurLogs::getJeuPlusPopulaire() const
{
    auto result = std::max_element(heuresJeux_.begin(), heuresJeux_.end(), ComparateurSecondElementPaire);
    if (result == heuresJeux_.end()) { return nullptr; }
    return heuresJeux_.at(result)->first;
}

int  AnalyseurLogs::getNombreSeancesPourUtilisateur(const Utilisateur*
    utilisateur) const
{
    std::count_if(logs_.begin(), logs_.end(), [&utilisateur](LigneLog) {return log.utilisateur == utilisateur; });
}
 
std::vector<const Jeu*> getJeuxJouesParUtilisateur(const
    Utilisateur* utilisateur) const
{
    std::unordered_set<const Jeu*> jeux;
    std::copy_if(logs_.begin(), logs_.end(), std::back_inserter(jeux), [&utilisateur](LigneLog log) {return log.utilisateur == utilisateur; jeux });
    std::vector<const Jeu*> jeu= cpy
    return 
}
