/// Fonctions auxiliaires à la struct pour les jeux.
/// \author Misha Krieger-Raynauld
/// \date 2020-10-29

#include "Jeu.h"
#include <unordered_map>

/// Convertit la valeur du enum Jeu::Genre en string.
/// \param genre    Le genre à convertir.
/// \return         String représentant le enum.
std::string getGenreString(Jeu::Genre genre)
{
    static const std::unordered_map<Jeu::Genre, std::string> genreStrings = {
        {Jeu::Genre::Fps, "First-person shooter"},
        {Jeu::Genre::Platformer, "Platformer"},
        {Jeu::Genre::Course, "Course"},
        {Jeu::Genre::Action, "Action"},
        {Jeu::Genre::Aventure, "Aventure"},
        {Jeu::Genre::Rpg, "Role-playing game"},
        {Jeu::Genre::Mmo, "MMO"},
        {Jeu::Genre::Simulation, "Simulation"},
        {Jeu::Genre::Sport, "Sport"},
        {Jeu::Genre::Autre, "Autre"},
    };

    auto it = genreStrings.find(genre);
    if (it != genreStrings.cend())
    {
        return it->second;
    }
    return "Erreur";
}

/// Affiche les informations d'un jeu à la sortie du stream donné.
/// \param outputStream Le stream auquel écrire les informations du jeu.
/// \param jeu          Le jeu à afficher au stream.
/// \return             Une référence au stream.
std::ostream& operator<<(std::ostream& outputStream, const Jeu& jeu)
{
    outputStream << "Nom: " << jeu.nom << " | Genre: " << getGenreString(jeu.genre)
                 << " | Développeur: " << jeu.developpeur << " | Année: " << jeu.annee;
    return outputStream;
}
