/// Tests automatisés.
/// \author Misha Krieger-Raynauld
/// \date 2020-10-29

#include "Tests.h"
#include <algorithm>
#include <array>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>
#include "AnalyseurLogs.h"
#include "Foncteurs.h"
#include "GestionnaireJeux.h"
#include "GestionnaireUtilisateurs.h"

namespace
{
    /// Affiche un header pour chaque section de tests à l'écran.
    /// \param nomSectionTest   Le nom de la section de tests.
    void afficherHeaderTest(const std::string& nomSectionTest)
    {
        std::cout << "\nTests pour " + nomSectionTest + ":\n--------\n";
    }

    /// Affiche un footer avec les points pour chaque section de tests à l'écran.
    /// \param totalPoints  Le nombre de points obtenus à la section de tests.
    /// \param maxPoints    Le nombre de points maximal alloués à la section de tests.
    void afficherFooterTest(double totalPoints, double maxPoints)
    {
        std::cout << "--------\nTotal pour la section: " << totalPoints << "/" << maxPoints << '\n';
    }

    /// Affiche le nom d'un test ainsi que son état de passage à l'écran.
    /// \param index        L'index du test.
    /// \param nom          Le nom du test.
    /// \param estReussi    L'état de passage du test.
    void afficherResultatTest(int index, const std::string& nom, bool estReussi)
    {
        static constexpr int largeurNumeroTest = 2;
        static constexpr int largeurNomTest = 50;
        std::cout << "Test " << std::right << std::setw(largeurNumeroTest) << index << ": " << std::left
                  << std::setw(largeurNomTest) << nom << ": " << (estReussi ? "OK" : "FAILED") << '\n';
    }
} // namespace

namespace Tests
{
    /// Appelle tous les tests et affiche la somme de ceux-ci à l'écran.
    void testAll()
    {
        static constexpr double maxPointsAll = 6.0;

        double totalPointsAll = 0.0;
        totalPointsAll += testGestionnaireUtilisateurs();
        totalPointsAll += testFoncteurs();
        totalPointsAll += testGestionnaireJeux();
        totalPointsAll += testAnalyseurLogs();

        std::cout << "\nTotal pour tous les tests: " << totalPointsAll << '/' << maxPointsAll << '\n';
    }

    /// Teste la classe GestionnaireUtilisateurs.
    /// \return Le nombre de points obtenus aux tests.
    double testGestionnaireUtilisateurs()
    {
        afficherHeaderTest("GestionnaireUtilisateurs");
        static constexpr double maxPointsSection = 1.0;

#if TEST_GESTIONNAIRE_UTILISATEURS_ACTIF
        std::vector<bool> tests;

        GestionnaireUtilisateurs gestionnaireUtilisateurs;

        // Test 1
        Utilisateur utilisateur1{"prénom.nom.1@email.com", "Prénom Nom", 20, Pays::Canada};
        Utilisateur utilisateur2{"prénom.nom.2@email.com", "Prénom Nom", 20, Pays::Canada};
        bool ajout1 = gestionnaireUtilisateurs.ajouterUtilisateur(utilisateur1);
        bool ajout2 = gestionnaireUtilisateurs.ajouterUtilisateur(utilisateur1);
        bool ajout3 = gestionnaireUtilisateurs.ajouterUtilisateur(utilisateur2);
        tests.push_back(ajout1 && !ajout2 && ajout3);
        afficherResultatTest(1, "GestionnaireUtilisateurs::ajouterUtilisateur", tests.back());

        // Test 2
        Utilisateur utilisateur3{"prénom.nom.3@email.com", "Prénom Nom", 20, Pays::Canada};
        bool suppression1 = gestionnaireUtilisateurs.supprimerUtilisateur(utilisateur1.id);
        bool suppression2 = gestionnaireUtilisateurs.supprimerUtilisateur(utilisateur1.id);
        bool suppression3 = gestionnaireUtilisateurs.supprimerUtilisateur(utilisateur2.id);
        bool suppression4 = gestionnaireUtilisateurs.supprimerUtilisateur(utilisateur3.id);
        tests.push_back(suppression1 && !suppression2 && suppression3 && !suppression4);
        afficherResultatTest(2, "GestionnaireUtilisateurs::supprimerUtilisateur", tests.back());

        // Test 3
        std::size_t nombre1 = gestionnaireUtilisateurs.getNombreUtilisateurs();
        gestionnaireUtilisateurs.ajouterUtilisateur(utilisateur1);
        gestionnaireUtilisateurs.ajouterUtilisateur(utilisateur2);
        gestionnaireUtilisateurs.ajouterUtilisateur(utilisateur3);
        std::size_t nombre2 = gestionnaireUtilisateurs.getNombreUtilisateurs();
        tests.push_back(nombre1 == 0 && nombre2 == 3);
        afficherResultatTest(3, "GestionnaireUtilisateurs::getNombreUtilisateurs", tests.back());

        // Test 4
        Utilisateur utilisateur4{"prénom.nom.4@email.com", "PrénomUnique NomUnique", 30, Pays::EtatsUnis};
        Utilisateur utilisateur5{"prénom.nom.5@email.com", "Prénom Nom", 20, Pays::Canada};
        gestionnaireUtilisateurs.ajouterUtilisateur(utilisateur4);
        const Utilisateur* utilisateurTrouve1 = gestionnaireUtilisateurs.getUtilisateurParId(utilisateur4.id);
        bool utilisateurTrouve1EstIdentique =
            utilisateurTrouve1 != nullptr && utilisateurTrouve1->id == utilisateur4.id &&
            utilisateurTrouve1->nom == utilisateur4.nom && utilisateurTrouve1->age == utilisateur4.age &&
            utilisateurTrouve1->pays == utilisateur4.pays;
        const Utilisateur* utilisateurTrouve2 = gestionnaireUtilisateurs.getUtilisateurParId(utilisateur5.id);
        gestionnaireUtilisateurs.supprimerUtilisateur(utilisateur4.id);
        const Utilisateur* utilisateurTrouve3 = gestionnaireUtilisateurs.getUtilisateurParId(utilisateur4.id);
        tests.push_back(utilisateurTrouve1EstIdentique && utilisateurTrouve2 == nullptr &&
                        utilisateurTrouve3 == nullptr);
        afficherResultatTest(4, "GestionnaireUtilisateurs::getUtilisateurParId", tests.back());

        // Test 5
        gestionnaireUtilisateurs.chargerDepuisFichier("utilisateurs.txt");
        std::stringstream stream;
        stream << gestionnaireUtilisateurs;
        std::vector<std::string> lignes;
        std::string ligneStream;
        while (std::getline(stream, ligneStream))
        {
            lignes.push_back(std::move(ligneStream));
        }
        std::sort(std::next(lignes.begin(), 1), lignes.end());
        std::string sortieRecue;
        for (const auto& ligne : lignes)
        {
            sortieRecue += ligne + '\n';
        }
        static const std::string sortieAttendue =
            "Le gestionnaire d'utilisateurs contient 100 utilisateurs:\n"
            "\tIdentifiant: akoblin@optonline.net | Nom: Marnie Melvin | Âge: 37 | Pays: Chine\n"
            "\tIdentifiant: alhajj@mac.com | Nom: Toccara Patino | Âge: 65 | Pays: Canada\n"
            "\tIdentifiant: andrei@hotmail.com | Nom: Armanda Hooper | Âge: 11 | Pays: Royaume-Uni\n"
            "\tIdentifiant: animats@sbcglobal.net | Nom: Nickole Montero | Âge: 91 | Pays: Mexique\n"
            "\tIdentifiant: augusto@verizon.net | Nom: Rhett Sargent | Âge: 15 | Pays: Mexique\n"
            "\tIdentifiant: barjam@sbcglobal.net | Nom: Kennith Doan | Âge: 69 | Pays: États-Unis\n"
            "\tIdentifiant: bartak@outlook.com | Nom: Eli Burdette | Âge: 86 | Pays: Russie\n"
            "\tIdentifiant: bebing@mac.com | Nom: Rupert Irvine | Âge: 9 | Pays: Mexique\n"
            "\tIdentifiant: bester@optonline.net | Nom: Margherita Noland | Âge: 50 | Pays: États-Unis\n"
            "\tIdentifiant: bsikdar@gmail.com | Nom: Emerald Stubbs | Âge: 5 | Pays: France\n"
            "\tIdentifiant: cantu@optonline.net | Nom: Amberly Bohannon | Âge: 99 | Pays: Brésil\n"
            "\tIdentifiant: carreras@mac.com | Nom: Roma Greco | Âge: 29 | Pays: Mexique\n"
            "\tIdentifiant: chaffar@live.com | Nom: Babara Guerin | Âge: 52 | Pays: Russie\n"
            "\tIdentifiant: claypool@aol.com | Nom: Hilton Coles | Âge: 90 | Pays: Mexique\n"
            "\tIdentifiant: codex@mac.com | Nom: Stan Durand | Âge: 3 | Pays: Royaume-Uni\n"
            "\tIdentifiant: cosimo@live.com | Nom: Ilana Cervantes | Âge: 71 | Pays: Canada\n"
            "\tIdentifiant: cremonini@verizon.net | Nom: Truman Sawyer | Âge: 20 | Pays: Mexique\n"
            "\tIdentifiant: danzigism@att.net | Nom: Masako Shipp | Âge: 44 | Pays: Chine\n"
            "\tIdentifiant: dawnsong@icloud.com | Nom: Stephine Jamison | Âge: 16 | Pays: Canada\n"
            "\tIdentifiant: denton@me.com | Nom: Vashti Snipes | Âge: 80 | Pays: Canada\n"
            "\tIdentifiant: dinther@me.com | Nom: Deetta Edmond | Âge: 94 | Pays: Chine\n"
            "\tIdentifiant: dkrishna@att.net | Nom: Shirely Kenney | Âge: 100 | Pays: Canada\n"
            "\tIdentifiant: dmouse@mac.com | Nom: Marlin Mcgregor | Âge: 51 | Pays: Chine\n"
            "\tIdentifiant: dogdude@icloud.com | Nom: Renetta Camarillo | Âge: 8 | Pays: Japon\n"
            "\tIdentifiant: dsowsy@yahoo.com | Nom: Angla Corrigan | Âge: 37 | Pays: Mexique\n"
            "\tIdentifiant: dvdotnet@yahoo.ca | Nom: Shirlee Keating | Âge: 82 | Pays: Canada\n"
            "\tIdentifiant: facet@verizon.net | Nom: Charisse Beckman | Âge: 73 | Pays: France\n"
            "\tIdentifiant: falcao@verizon.net | Nom: Breanne Hawley | Âge: 86 | Pays: Brésil\n"
            "\tIdentifiant: fatelk@comcast.net | Nom: Gay Shuler | Âge: 71 | Pays: États-Unis\n"
            "\tIdentifiant: frode@sbcglobal.net | Nom: Lane Britton | Âge: 25 | Pays: Brésil\n"
            "\tIdentifiant: froodian@mac.com | Nom: Florrie Carmichael | Âge: 50 | Pays: Brésil\n"
            "\tIdentifiant: fwitness@gmail.com | Nom: Arlinda Murillo | Âge: 62 | Pays: Japon\n"
            "\tIdentifiant: gomor@optonline.net | Nom: Earlie Portillo | Âge: 57 | Pays: États-Unis\n"
            "\tIdentifiant: gravyface@live.com | Nom: Euna Spinks | Âge: 49 | Pays: Canada\n"
            "\tIdentifiant: grdschl@icloud.com | Nom: Lavenia Beach | Âge: 10 | Pays: Mexique\n"
            "\tIdentifiant: gward@sbcglobal.net | Nom: Rocco Lacroix | Âge: 76 | Pays: Russie\n"
            "\tIdentifiant: ijackson@gmail.com | Nom: Seymour Knudson | Âge: 28 | Pays: Japon\n"
            "\tIdentifiant: ivoibs@yahoo.ca | Nom: Louanne Bellamy | Âge: 83 | Pays: Russie\n"
            "\tIdentifiant: ivoibs@yahoo.com | Nom: Jenise Oates | Âge: 45 | Pays: Russie\n"
            "\tIdentifiant: jdhedden@mac.com | Nom: Meghann Cope | Âge: 80 | Pays: Royaume-Uni\n"
            "\tIdentifiant: jespley@me.com | Nom: Marylouise Otero | Âge: 26 | Pays: France\n"
            "\tIdentifiant: jfinke@comcast.net | Nom: Mara Yarbrough | Âge: 68 | Pays: Royaume-Uni\n"
            "\tIdentifiant: jkegl@verizon.net | Nom: Retta Wheatley | Âge: 91 | Pays: Brésil\n"
            "\tIdentifiant: kalpol@icloud.com | Nom: Chaya Massie | Âge: 21 | Pays: Mexique\n"
            "\tIdentifiant: karasik@hotmail.com | Nom: Luis Noe | Âge: 18 | Pays: États-Unis\n"
            "\tIdentifiant: karasik@msn.com | Nom: Carman Gipson | Âge: 97 | Pays: Russie\n"
            "\tIdentifiant: klaudon@hotmail.com | Nom: Salena Christy | Âge: 39 | Pays: Royaume-Uni\n"
            "\tIdentifiant: kobayasi@yahoo.com | Nom: Lindsay Lamar | Âge: 44 | Pays: France\n"
            "\tIdentifiant: kourai@verizon.net | Nom: Kendall Dias | Âge: 84 | Pays: Chine\n"
            "\tIdentifiant: kramulous@aol.com | Nom: Shizuko Beltran | Âge: 48 | Pays: Brésil\n"
            "\tIdentifiant: kwilliams@msn.com | Nom: Emmanuel Mcwhorter | Âge: 31 | Pays: Japon\n"
            "\tIdentifiant: leakin@yahoo.ca | Nom: Gita Stitt | Âge: 72 | Pays: Russie\n"
            "\tIdentifiant: linuxhack@yahoo.com | Nom: Lennie Hatley | Âge: 43 | Pays: Royaume-Uni\n"
            "\tIdentifiant: mastinfo@live.com | Nom: Echo Shumate | Âge: 41 | Pays: États-Unis\n"
            "\tIdentifiant: mbswan@optonline.net | Nom: Pattie Delgadillo | Âge: 84 | Pays: Canada\n"
            "\tIdentifiant: mcraigw@verizon.net | Nom: Anjanette Seitz | Âge: 58 | Pays: France\n"
            "\tIdentifiant: mcsporran@yahoo.ca | Nom: Anya Baez | Âge: 73 | Pays: Canada\n"
            "\tIdentifiant: melnik@aol.com | Nom: Isiah Peralta | Âge: 50 | Pays: Canada\n"
            "\tIdentifiant: mfburgo@live.com | Nom: Jaimee Chester | Âge: 36 | Pays: Royaume-Uni\n"
            "\tIdentifiant: mglee@outlook.com | Nom: Sharron Parson | Âge: 67 | Pays: Royaume-Uni\n"
            "\tIdentifiant: mlewan@hotmail.com | Nom: Tambra Wilbanks | Âge: 59 | Pays: Russie\n"
            "\tIdentifiant: moonlapse@msn.com | Nom: Verdell Reichert | Âge: 25 | Pays: Chine\n"
            "\tIdentifiant: mrobshaw@outlook.com | Nom: Joella Sparkman | Âge: 43 | Pays: Brésil\n"
            "\tIdentifiant: mschwartz@verizon.net | Nom: Lilly Valles | Âge: 84 | Pays: Brésil\n"
            "\tIdentifiant: mxiao@verizon.net | Nom: Marla Dempsey | Âge: 39 | Pays: États-Unis\n"
            "\tIdentifiant: notaprguy@verizon.net | Nom: Kacie Heflin | Âge: 80 | Pays: Canada\n"
            "\tIdentifiant: noticias@optonline.net | Nom: Adelaide Blank | Âge: 10 | Pays: Canada\n"
            "\tIdentifiant: pajas@comcast.net | Nom: Eldora Schaffer | Âge: 66 | Pays: Royaume-Uni\n"
            "\tIdentifiant: pakaste@gmail.com | Nom: Corazon Toliver | Âge: 2 | Pays: France\n"
            "\tIdentifiant: paley@msn.com | Nom: Magan Messer | Âge: 57 | Pays: France\n"
            "\tIdentifiant: parksh@msn.com | Nom: Alden Tyner | Âge: 21 | Pays: Chine\n"
            "\tIdentifiant: pdbaby@msn.com | Nom: Chas Spain | Âge: 27 | Pays: Royaume-Uni\n"
            "\tIdentifiant: philb@yahoo.ca | Nom: Hildred Fernandes | Âge: 89 | Pays: Chine\n"
            "\tIdentifiant: philen@msn.com | Nom: Gaston Hundley | Âge: 2 | Pays: Chine\n"
            "\tIdentifiant: plover@outlook.com | Nom: Julianne Sasser | Âge: 87 | Pays: Brésil\n"
            "\tIdentifiant: policies@yahoo.com | Nom: Fatimah Teal | Âge: 2 | Pays: Brésil\n"
            "\tIdentifiant: quinn@comcast.net | Nom: Lashell Bower | Âge: 54 | Pays: Russie\n"
            "\tIdentifiant: rasca@msn.com | Nom: Donnette Crawley | Âge: 76 | Pays: Canada\n"
            "\tIdentifiant: rddesign@me.com | Nom: Chanel Arevalo | Âge: 30 | Pays: Mexique\n"
            "\tIdentifiant: rgarton@att.net | Nom: Christiana Alves | Âge: 82 | Pays: France\n"
            "\tIdentifiant: rgiersig@live.com | Nom: Pierre Mcnabb | Âge: 28 | Pays: Chine\n"
            "\tIdentifiant: scottlee@att.net | Nom: Kiley Tang | Âge: 1 | Pays: Japon\n"
            "\tIdentifiant: sharon@icloud.com | Nom: Karine Herrington | Âge: 97 | Pays: Canada\n"
            "\tIdentifiant: singh@me.com | Nom: Tanika Weston | Âge: 76 | Pays: Canada\n"
            "\tIdentifiant: snunez@optonline.net | Nom: Mitchel Brandt | Âge: 2 | Pays: États-Unis\n"
            "\tIdentifiant: sopwith@hotmail.com | Nom: Russ Humphries | Âge: 98 | Pays: France\n"
            "\tIdentifiant: srour@me.com | Nom: Johnetta Adler | Âge: 72 | Pays: Chine\n"
            "\tIdentifiant: srour@verizon.net | Nom: Cyndi Marino | Âge: 42 | Pays: Russie\n"
            "\tIdentifiant: staikos@optonline.net | Nom: Christinia Rousseau | Âge: 14 | Pays: Mexique\n"
            "\tIdentifiant: temmink@att.net | Nom: Tracee Boyer | Âge: 97 | Pays: États-Unis\n"
            "\tIdentifiant: thrymm@aol.com | Nom: Olin Staton | Âge: 58 | Pays: Chine\n"
            "\tIdentifiant: tlinden@outlook.com | Nom: Thu Doughty | Âge: 22 | Pays: Canada\n"
            "\tIdentifiant: trieuvan@att.net | Nom: Mariana Pfeiffer | Âge: 85 | Pays: Chine\n"
            "\tIdentifiant: tubajon@live.com | Nom: Irish Gilmore | Âge: 21 | Pays: États-Unis\n"
            "\tIdentifiant: unreal@aol.com | Nom: Necole Johansen | Âge: 80 | Pays: Royaume-Uni\n"
            "\tIdentifiant: wainwrig@aol.com | Nom: Danyelle Craddock | Âge: 16 | Pays: Chine\n"
            "\tIdentifiant: wetter@yahoo.com | Nom: Mignon Haag | Âge: 69 | Pays: Canada\n"
            "\tIdentifiant: wikinerd@icloud.com | Nom: Georgetta Aquino | Âge: 10 | Pays: Japon\n"
            "\tIdentifiant: wortmanj@me.com | Nom: Bernie Napier | Âge: 1 | Pays: France\n"
            "\tIdentifiant: zyghom@yahoo.com | Nom: Easter Findley | Âge: 74 | Pays: Japon\n";
        tests.push_back(sortieRecue == sortieAttendue);
        afficherResultatTest(5, "GestionnaireUtilisateurs::operator<<", tests.back());

        int nombreTestsReussis = static_cast<int>(std::count(tests.begin(), tests.end(), true));
        double totalPointsSection =
            static_cast<double>(nombreTestsReussis) / static_cast<double>(tests.size()) * maxPointsSection;
#else
        std::cout << "[Tests désactivés]\n";
        double totalPointsSection = 0.0;
#endif
        afficherFooterTest(totalPointsSection, maxPointsSection);
        return totalPointsSection;
    }

    /// Teste les foncteurs du fichier Foncteurs.h.
    /// \return Le nombre de points obtenus aux tests.
    double testFoncteurs()
    {
        afficherHeaderTest("Foncteurs");
        static constexpr double maxPointsSection = 1.0;

#if TEST_FONCTEURS_ACTIF
        std::vector<bool> tests;

        // Test 1
        auto jeu1 = std::make_unique<Jeu>(Jeu{"Nom", Jeu::Genre::Fps, "Développeur", 1970});
        auto jeu2 = std::make_unique<Jeu>(Jeu{"Nom", Jeu::Genre::Fps, "Développeur", 1980});
        auto jeu3 = std::make_unique<Jeu>(Jeu{"Nom", Jeu::Genre::Fps, "Développeur", 1981});
        auto jeu4 = std::make_unique<Jeu>(Jeu{"Nom", Jeu::Genre::Fps, "Développeur", 1990});
        auto jeu5 = std::make_unique<Jeu>(Jeu{"Nom", Jeu::Genre::Fps, "Développeur", 1999});
        auto jeu6 = std::make_unique<Jeu>(Jeu{"Nom", Jeu::Genre::Fps, "Développeur", 2000});
        auto jeu7 = std::make_unique<Jeu>(Jeu{"Nom", Jeu::Genre::Fps, "Développeur", 2010});
        const auto jeu8 = std::make_unique<Jeu>(Jeu{"Nom", Jeu::Genre::Fps, "Développeur", 1995});
        int anneeInferieure = 1980;
        int anneeSuperieure = 2000;
        EstDansIntervalleDatesJeu foncteurIntervalle(anneeInferieure, anneeSuperieure);
        bool estDansIntervalle1 = foncteurIntervalle(jeu1);
        bool estDansIntervalle2 = foncteurIntervalle(jeu2);
        bool estDansIntervalle3 = foncteurIntervalle(jeu3);
        bool estDansIntervalle4 = foncteurIntervalle(jeu4);
        bool estDansIntervalle5 = foncteurIntervalle(jeu5);
        bool estDansIntervalle6 = foncteurIntervalle(jeu6);
        bool estDansIntervalle7 = foncteurIntervalle(jeu7);
        bool estDansIntervalle8 = foncteurIntervalle(jeu8);
        tests.push_back(!estDansIntervalle1 && estDansIntervalle2 && estDansIntervalle3 && estDansIntervalle4 &&
                        estDansIntervalle5 && estDansIntervalle6 && !estDansIntervalle7 && estDansIntervalle8);
        afficherResultatTest(1, "Foncteur EstDansIntervalleDatesJeu", tests.back());

        // Test 2
        Utilisateur utilisateur{"prénom.nom.@email.com", "Prénom Nom", 20, Pays::Canada};
        Jeu jeu{"Nom", Jeu::Genre::Fps, "Développeur", 1970};
        LigneLog ligneLog1{"2018-01-01T14:54:19Z", &utilisateur, &jeu, 1};
        LigneLog ligneLog2{"2018-04-01T14:54:19Z", &utilisateur, &jeu, 1};
        const LigneLog ligneLog3{"2018-06-01T14:54:19Z", &utilisateur, &jeu, 1};
        ComparateurLog foncteurComparateurLog;
        auto comparaisonLog1 = foncteurComparateurLog(ligneLog1, ligneLog2);
        auto comparaisonLog2 = foncteurComparateurLog(ligneLog2, ligneLog1);
        auto comparaisonLog3 = foncteurComparateurLog(ligneLog3, ligneLog3);
        tests.push_back(comparaisonLog1 && !comparaisonLog2 && !comparaisonLog3);
        afficherResultatTest(2, "Foncteur ComparateurLog", tests.back());

        // Test 3
        std::pair<std::string, int> pair1("First", 2);
        std::pair<std::string, int> pair2("First", 3);
        const std::pair<std::string, int> pair3("First", 4);
        ComparateurSecondElementPaire<std::string, int> foncteurComparateurSecondElementPaire;
        auto comparaisonPaire1 = foncteurComparateurSecondElementPaire(pair1, pair2);
        auto comparaisonPaire2 = foncteurComparateurSecondElementPaire(pair2, pair1);
        auto comparaisonPaire3 = foncteurComparateurSecondElementPaire(pair3, pair3);
        tests.push_back(comparaisonPaire1 && !comparaisonPaire2 && !comparaisonPaire3);
        afficherResultatTest(3, "Foncteur ComparateurSecondElementPaire", tests.back());

        int nombreTestsReussis = static_cast<int>(std::count(tests.begin(), tests.end(), true));
        double totalPointsSection =
            static_cast<double>(nombreTestsReussis) / static_cast<double>(tests.size()) * maxPointsSection;
#else
        std::cout << "[Tests désactivés]\n";
        double totalPointsSection = 0.0;
#endif
        afficherFooterTest(totalPointsSection, maxPointsSection);
        return totalPointsSection;
    }

    /// Teste la classe GestionnaireJeux.
    /// \return Le nombre de points obtenus aux tests.
    double testGestionnaireJeux()
    {
        afficherHeaderTest("GestionnaireJeux");
        static constexpr double maxPointsSection = 2.0;

#if TEST_GESTIONNAIRE_JEUX_ACTIF
        std::vector<bool> tests;

        GestionnaireJeux gestionnaireJeux;

        // Test 1
        Jeu jeu1{"Nom1", Jeu::Genre::Fps, "Développeur", 1970};
        Jeu jeu2{"Nom2", Jeu::Genre::Fps, "Développeur", 1970};
        bool ajout1 = gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu1));
        std::vector<const Jeu*> vecteurFiltresEtatAjout1({gestionnaireJeux.jeux_[0].get()});
        bool ajout1EtatValide = gestionnaireJeux.jeux_.size() == 1 &&
                                gestionnaireJeux.filtreNomJeux_[jeu1.nom] == gestionnaireJeux.jeux_[0].get() &&
                                gestionnaireJeux.filtreGenreJeux_[jeu1.genre] == vecteurFiltresEtatAjout1 &&
                                gestionnaireJeux.filtreDeveloppeurJeux_[jeu1.developpeur] == vecteurFiltresEtatAjout1;
        bool ajout2 = gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu1));
        bool ajout3 = gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu2));
        std::vector<const Jeu*> vecteurFiltresEtatAjout3(
            {gestionnaireJeux.jeux_[0].get(), gestionnaireJeux.jeux_[1].get()});
        bool ajout3EtatValide = gestionnaireJeux.jeux_.size() == 2 &&
                                gestionnaireJeux.filtreNomJeux_[jeu2.nom] == gestionnaireJeux.jeux_[1].get() &&
                                gestionnaireJeux.filtreGenreJeux_[jeu2.genre] == vecteurFiltresEtatAjout3 &&
                                gestionnaireJeux.filtreDeveloppeurJeux_[jeu2.developpeur] == vecteurFiltresEtatAjout3;
        tests.push_back(ajout1 && ajout1EtatValide && !ajout2 && ajout3 && ajout3EtatValide);
        afficherResultatTest(1, "GestionnaireJeux::ajouterJeu", tests.back());

        // Test 2
        Jeu jeu3{"Nom3", Jeu::Genre::Fps, "Développeur", 1970};
        std::vector<const Jeu*> vecteurFiltresEtatSuppression1({gestionnaireJeux.jeux_[1].get()});
        bool suppression1 = gestionnaireJeux.supprimerJeu(jeu1.nom);
        bool suppression1EtatValide =
            gestionnaireJeux.jeux_.size() == 1 &&
            gestionnaireJeux.filtreNomJeux_.find(jeu1.nom) == gestionnaireJeux.filtreNomJeux_.end() &&
            gestionnaireJeux.filtreGenreJeux_[jeu1.genre] == vecteurFiltresEtatSuppression1 &&
            gestionnaireJeux.filtreDeveloppeurJeux_[jeu1.developpeur] == vecteurFiltresEtatSuppression1;
        bool suppression2 = gestionnaireJeux.supprimerJeu(jeu1.nom);
        bool suppression3 = gestionnaireJeux.supprimerJeu(jeu2.nom);
        bool suppression3EtatValide =
            gestionnaireJeux.jeux_.empty() &&
            gestionnaireJeux.filtreNomJeux_.find(jeu2.nom) == gestionnaireJeux.filtreNomJeux_.end() &&
            gestionnaireJeux.filtreGenreJeux_[jeu2.genre].empty() &&
            gestionnaireJeux.filtreDeveloppeurJeux_[jeu2.developpeur].empty();
        bool suppression4 = gestionnaireJeux.supprimerJeu(jeu3.nom);
        tests.push_back(suppression1 && suppression1EtatValide && !suppression2 && suppression3 &&
                        suppression3EtatValide && !suppression4);
        afficherResultatTest(2, "GestionnaireJeux::supprimerJeu", tests.back());

        // Test 3
        std::size_t nombre1 = gestionnaireJeux.getNombreJeux();
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu1));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu2));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu3));
        std::size_t nombre2 = gestionnaireJeux.getNombreJeux();
        tests.push_back(nombre1 == 0 && nombre2 == 3);
        afficherResultatTest(3, "GestionnaireJeux::getNombreJeux", tests.back());

        // Test 4
        Jeu jeu4{"Nom4", Jeu::Genre::Platformer, "DéveloppeurUnique", 1920};
        Jeu jeu5{"Nom5", Jeu::Genre::Fps, "Développeur", 1970};
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu4));
        const Jeu* jeuTrouve1 = gestionnaireJeux.getJeuParNom(jeu4.nom);
        bool jeuTrouve1EstIdentique = jeuTrouve1 != nullptr && jeuTrouve1->nom == jeu4.nom &&
                                      jeuTrouve1->genre == jeu4.genre && jeuTrouve1->developpeur == jeu4.developpeur &&
                                      jeuTrouve1->annee == jeu4.annee;
        const Jeu* jeuTrouve2 = gestionnaireJeux.getJeuParNom(jeu5.nom);
        gestionnaireJeux.supprimerJeu(jeu4.nom);
        const Jeu* jeuTrouve3 = gestionnaireJeux.getJeuParNom(jeu4.nom);
        tests.push_back(jeuTrouve1EstIdentique && jeuTrouve2 == nullptr && jeuTrouve3 == nullptr);
        afficherResultatTest(4, "GestionnaireJeux::getJeuParNom", tests.back());

        // Test 5
        std::vector<const Jeu*> jeuxParGenre1 = gestionnaireJeux.getJeuxParGenre(Jeu::Genre::Course);
        Jeu jeu6{"Nom6", Jeu::Genre::Aventure, "Développeur", 1970};
        Jeu jeu7{"Nom7", Jeu::Genre::Aventure, "Développeur", 1970};
        Jeu jeu8{"Nom8", Jeu::Genre::Aventure, "Développeur", 1970};
        Jeu jeu9{"Nom9", Jeu::Genre::Aventure, "Développeur", 1970};
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu6));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu7));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu8));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu9));
        const Jeu* pointeurJeu6 = gestionnaireJeux.getJeuParNom(jeu6.nom);
        const Jeu* pointeurJeu7 = gestionnaireJeux.getJeuParNom(jeu7.nom);
        const Jeu* pointeurJeu8 = gestionnaireJeux.getJeuParNom(jeu8.nom);
        const Jeu* pointeurJeu9 = gestionnaireJeux.getJeuParNom(jeu9.nom);
        std::vector<const Jeu*> jeuxParGenre2 = gestionnaireJeux.getJeuxParGenre(Jeu::Genre::Aventure);
        std::vector<const Jeu*> jeuxParGenre2Attendus = {pointeurJeu6, pointeurJeu7, pointeurJeu8, pointeurJeu9};
        bool jeuxParGenre2Valides = jeuxParGenre2 == jeuxParGenre2Attendus;
        gestionnaireJeux.supprimerJeu(jeu7.nom);
        std::vector<const Jeu*> jeuxParGenre3 = gestionnaireJeux.getJeuxParGenre(Jeu::Genre::Aventure);
        std::vector<const Jeu*> jeuxParGenre3Attendus = {pointeurJeu6, pointeurJeu8, pointeurJeu9};
        bool jeuxParGenre3Valides = jeuxParGenre3 == jeuxParGenre3Attendus;
        tests.push_back(jeuxParGenre1.empty() && jeuxParGenre2Valides && jeuxParGenre3Valides);
        afficherResultatTest(5, "GestionnaireJeux::getJeuxParGenre", tests.back());

        // Test 6
        std::vector<const Jeu*> jeuxParDeveloppeur1 = gestionnaireJeux.getJeuxParDeveloppeur("DéveloppeurUnique1");
        Jeu jeu10{"Nom10", Jeu::Genre::Fps, "DéveloppeurUnique2", 1970};
        Jeu jeu11{"Nom11", Jeu::Genre::Fps, "DéveloppeurUnique2", 1970};
        Jeu jeu12{"Nom12", Jeu::Genre::Fps, "DéveloppeurUnique2", 1970};
        Jeu jeu13{"Nom13", Jeu::Genre::Fps, "DéveloppeurUnique2", 1970};
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu10));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu11));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu12));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu13));
        const Jeu* pointeurJeu10 = gestionnaireJeux.getJeuParNom(jeu10.nom);
        const Jeu* pointeurJeu11 = gestionnaireJeux.getJeuParNom(jeu11.nom);
        const Jeu* pointeurJeu12 = gestionnaireJeux.getJeuParNom(jeu12.nom);
        const Jeu* pointeurJeu13 = gestionnaireJeux.getJeuParNom(jeu13.nom);
        std::vector<const Jeu*> jeuxParDeveloppeur2 = gestionnaireJeux.getJeuxParDeveloppeur("DéveloppeurUnique2");
        std::vector<const Jeu*> jeuxParDeveloppeur2Attendus = {pointeurJeu10,
                                                               pointeurJeu11,
                                                               pointeurJeu12,
                                                               pointeurJeu13};
        bool jeuxParDeveloppeur2Valides = jeuxParDeveloppeur2 == jeuxParDeveloppeur2Attendus;
        gestionnaireJeux.supprimerJeu(jeu11.nom);
        std::vector<const Jeu*> jeuxParDeveloppeur3 = gestionnaireJeux.getJeuxParDeveloppeur("DéveloppeurUnique2");
        std::vector<const Jeu*> jeuxParDeveloppeur3Attendus = {pointeurJeu10, pointeurJeu12, pointeurJeu13};
        bool jeuxParDeveloppeur3Valides = jeuxParDeveloppeur3 == jeuxParDeveloppeur3Attendus;
        tests.push_back(jeuxParDeveloppeur1.empty() && jeuxParDeveloppeur2Valides && jeuxParDeveloppeur3Valides);
        afficherResultatTest(6, "GestionnaireJeux::getJeuxParDeveloppeur", tests.back());

        // Test 7
        std::vector<const Jeu*> jeuxEntreAnnees1 = gestionnaireJeux.getJeuxEntreAnnees(1000, 1100);
        Jeu jeu14{"Nom14", Jeu::Genre::Fps, "Développeur", 1970};
        Jeu jeu15{"Nom15", Jeu::Genre::Fps, "Développeur", 1980};
        Jeu jeu16{"Nom16", Jeu::Genre::Fps, "Développeur", 1985};
        Jeu jeu17{"Nom17", Jeu::Genre::Fps, "Développeur", 1995};
        Jeu jeu18{"Nom18", Jeu::Genre::Fps, "Développeur", 2000};
        Jeu jeu19{"Nom19", Jeu::Genre::Fps, "Développeur", 2010};
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu14));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu15));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu16));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu17));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu18));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu19));
        const Jeu* pointeurJeu15 = gestionnaireJeux.getJeuParNom(jeu15.nom);
        const Jeu* pointeurJeu16 = gestionnaireJeux.getJeuParNom(jeu16.nom);
        const Jeu* pointeurJeu17 = gestionnaireJeux.getJeuParNom(jeu17.nom);
        const Jeu* pointeurJeu18 = gestionnaireJeux.getJeuParNom(jeu18.nom);
        std::vector<const Jeu*> jeuxEntreAnnees2 = gestionnaireJeux.getJeuxEntreAnnees(1980, 2000);
        std::vector<const Jeu*> jeuxEntreAnnees2Attendus = {pointeurJeu15, pointeurJeu16, pointeurJeu17, pointeurJeu18};
        gestionnaireJeux.supprimerJeu(jeu16.nom);
        bool jeuxEntreAnnees2Valides = jeuxEntreAnnees2 == jeuxEntreAnnees2Attendus;
        std::vector<const Jeu*> jeuxEntreAnnees3 = gestionnaireJeux.getJeuxEntreAnnees(1980, 2000);
        std::vector<const Jeu*> jeuxEntreAnnees3Attendus = {pointeurJeu15, pointeurJeu17, pointeurJeu18};
        bool jeuxEntreAnnees3Valides = jeuxEntreAnnees3 == jeuxEntreAnnees3Attendus;
        tests.push_back(jeuxEntreAnnees1.empty() && jeuxEntreAnnees2Valides && jeuxEntreAnnees3Valides);
        afficherResultatTest(7, "GestionnaireJeux::getJeuxEntreAnnees", tests.back());

        // Test 8
        gestionnaireJeux = GestionnaireJeux(); // Réinitialiser le gestionnaire de jeux
        Jeu jeu20{"Nom20", Jeu::Genre::Fps, "Développeur", 1970};
        Jeu jeu21{"Nom21", Jeu::Genre::Fps, "Développeur", 1970};
        Jeu jeu22{"Nom22", Jeu::Genre::Fps, "Développeur", 1970};
        Jeu jeu23{"Nom23", Jeu::Genre::Fps, "Développeur", 1970};
        Jeu jeu24{"Nom24", Jeu::Genre::Fps, "Développeur", 1970};
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu20));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu21));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu22));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu23));
        gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(jeu24));
        std::ostringstream stream;
        stream << gestionnaireJeux;
        static const std::string sortieAttendue =
            "Le gestionnaire de jeux contient 5 jeux.\n"
            "Affichage par catégories:\n"
            "Genre: First-person shooter (5 jeux):\n"
            "\tNom: Nom20 | Genre: First-person shooter | Développeur: Développeur | Année: 1970\n"
            "\tNom: Nom21 | Genre: First-person shooter | Développeur: Développeur | Année: 1970\n"
            "\tNom: Nom22 | Genre: First-person shooter | Développeur: Développeur | Année: 1970\n"
            "\tNom: Nom23 | Genre: First-person shooter | Développeur: Développeur | Année: 1970\n"
            "\tNom: Nom24 | Genre: First-person shooter | Développeur: Développeur | Année: 1970\n";
        tests.push_back(stream.str() == sortieAttendue);
        afficherResultatTest(8, "GestionnaireJeux::operator<<", tests.back());

        // Test 9
        GestionnaireJeux gestionnaireJeux2(gestionnaireJeux);
        std::size_t nombre3 = gestionnaireJeux2.getNombreJeux();
        gestionnaireJeux2.chargerDepuisFichier("jeux.txt");
        std::size_t nombre4 = gestionnaireJeux2.getNombreJeux();
        tests.push_back(nombre3 == gestionnaireJeux.getNombreJeux() && nombre4 == 278);
        afficherResultatTest(9, "Chargement et copy ctor toujours fonctionnels", tests.back());

        int nombreTestsReussis = static_cast<int>(std::count(tests.begin(), tests.end(), true));
        double totalPointsSection =
            static_cast<double>(nombreTestsReussis) / static_cast<double>(tests.size()) * maxPointsSection;
#else
        std::cout << "[Tests désactivés]\n";
        double totalPointsSection = 0.0;
#endif
        afficherFooterTest(totalPointsSection, maxPointsSection);
        return totalPointsSection;
    }

    /// Teste la classe AnalyseurLogs.
    /// \return Le nombre de points obtenus aux tests.
    double testAnalyseurLogs()
    {
        afficherHeaderTest("AnalyseurLogs");
        static constexpr double maxPointsSection = 2.0;

#if TEST_ANALYSEUR_LOGS_ACTIF
        std::vector<bool> tests;

        GestionnaireUtilisateurs gestionnaireUtilisateurs;
        static constexpr std::size_t nombreUtilisateurs = 10;
        std::array<const Utilisateur*, nombreUtilisateurs> pointeursUtilisateurs;
        for (std::size_t i = 0; i < nombreUtilisateurs; i++)
        {
            std::string idUtilisateur = "prénom.nom." + std::to_string(i + 1) + "@email.com";
            gestionnaireUtilisateurs.ajouterUtilisateur(Utilisateur{idUtilisateur, "Prénom Nom", 20, Pays::Canada});
            pointeursUtilisateurs[i] = gestionnaireUtilisateurs.getUtilisateurParId(idUtilisateur);
        }

        GestionnaireJeux gestionnaireJeux;
        static constexpr std::size_t nombreJeux = 10;
        std::array<const Jeu*, nombreJeux> pointeursJeux;
        for (std::size_t i = 0; i < nombreJeux; i++)
        {
            std::string nomJeu = "Nom" + std::to_string(i + 1);
            gestionnaireJeux.ajouterJeu(std::make_unique<Jeu>(Jeu{nomJeu, Jeu::Genre::Fps, "Développeur", 1970}));
            pointeursJeux[i] = gestionnaireJeux.getJeuParNom(nomJeu);
        }

        AnalyseurLogs analyseurLogs;

        // Test 1
        LigneLog ligneLog1 = analyseurLogs.creerLigneLog("2018-01-01T00:00:00Z",
                                                         "inconnu@email.com",
                                                         "Inconnu",
                                                         1,
                                                         gestionnaireUtilisateurs,
                                                         gestionnaireJeux);
        LigneLog ligneLog2 = analyseurLogs.creerLigneLog("2018-01-01T00:00:00Z",
                                                         "prénom.nom.1@email.com",
                                                         "Nom1",
                                                         2,
                                                         gestionnaireUtilisateurs,
                                                         gestionnaireJeux);
        bool ligneLog1Valide = ligneLog1.timestamp == "2018-01-01T00:00:00Z" && ligneLog1.utilisateur == nullptr &&
                               ligneLog1.jeu == nullptr && ligneLog1.heures == 1;
        bool ligneLog2Valide = ligneLog2.timestamp == "2018-01-01T00:00:00Z" &&
                               ligneLog2.utilisateur == pointeursUtilisateurs[0] && ligneLog2.jeu == pointeursJeux[0] &&
                               ligneLog2.heures == 2;
        tests.push_back(ligneLog1Valide && ligneLog2Valide);
        afficherResultatTest(1, "AnalyseurLogs::creerLigneLog (private)", tests.back());

        // Test 2
        std::vector<LigneLog> logsAjoutes = {
            LigneLog{"2018-01-01T00:00:00Z", pointeursUtilisateurs[3], pointeursJeux[4], 1},
            LigneLog{"2018-01-01T12:00:00Z", pointeursUtilisateurs[3], pointeursJeux[4], 2},
            LigneLog{"2018-01-01T11:00:00Z", pointeursUtilisateurs[0], pointeursJeux[4], 1},
            LigneLog{"2018-01-01T10:00:00Z", pointeursUtilisateurs[1], pointeursJeux[4], 2},
            LigneLog{"2018-01-01T09:00:00Z", pointeursUtilisateurs[2], pointeursJeux[4], 4},
            LigneLog{"2018-01-01T08:00:00Z", pointeursUtilisateurs[3], pointeursJeux[8], 4},
            LigneLog{"2018-01-01T07:00:00Z", pointeursUtilisateurs[4], pointeursJeux[8], 8},
            LigneLog{"2018-01-01T06:00:00Z", pointeursUtilisateurs[5], pointeursJeux[8], 3},
            LigneLog{"2018-01-01T05:00:00Z", pointeursUtilisateurs[6], pointeursJeux[9], 1},
            LigneLog{"2018-01-01T04:00:00Z", pointeursUtilisateurs[7], pointeursJeux[9], 1},
            LigneLog{"2018-01-01T03:00:00Z", pointeursUtilisateurs[8], pointeursJeux[0], 1},
            LigneLog{"2018-01-01T02:00:00Z", pointeursUtilisateurs[9], pointeursJeux[1], 3},
            LigneLog{"2018-01-01T01:00:00Z", pointeursUtilisateurs[1], pointeursJeux[2], 2},
            LigneLog{"2018-01-01T01:00:00Z", pointeursUtilisateurs[1], pointeursJeux[3], 9},
            LigneLog{"2018-01-01T01:00:00Z", pointeursUtilisateurs[1], pointeursJeux[4], 2},
            LigneLog{"2019-01-01T01:00:00Z", pointeursUtilisateurs[3], pointeursJeux[5], 2},
            LigneLog{"2020-01-01T01:00:00Z", pointeursUtilisateurs[3], pointeursJeux[6], 1},
            LigneLog{"2019-05-01T01:00:00Z", pointeursUtilisateurs[3], pointeursJeux[7], 1},
            LigneLog{"2020-05-01T01:00:00Z", pointeursUtilisateurs[3], pointeursJeux[8], 5},
            LigneLog{"2019-03-01T01:00:00Z", pointeursUtilisateurs[1], pointeursJeux[9], 6},
        };
        analyseurLogs = AnalyseurLogs(); // Réinitialiser l'analyseur de logs
        bool ajoutsValides1 = true;
        for (const auto& ligneLog : logsAjoutes)
        {
            ajoutsValides1 = analyseurLogs.ajouterLigneLogNonTriee(ligneLog) && ajoutsValides1;
        }
        LigneLog ligneLogInvalide1 = LigneLog{"2018-01-01T00:00:00Z", nullptr, pointeursJeux[0], 1};
        bool ajoutInvalide1 = analyseurLogs.ajouterLigneLogNonTriee(ligneLogInvalide1);
        LigneLog ligneLogInvalide2 = LigneLog{"2018-01-01T00:00:00Z", pointeursUtilisateurs[0], nullptr, 1};
        bool ajoutInvalide2 = analyseurLogs.ajouterLigneLogNonTriee(ligneLogInvalide2);
        int heuresJoueesJeu1 = analyseurLogs.heuresJeux_[pointeursJeux[4]];
        int heuresJoueesJeu2 = analyseurLogs.heuresJeux_[pointeursJeux[5]];
        tests.push_back(ajoutsValides1 && !ajoutInvalide1 && !ajoutInvalide2 &&
                        analyseurLogs.logs_.back().timestamp == logsAjoutes.back().timestamp &&
                        heuresJoueesJeu1 == 12 && heuresJoueesJeu2 == 2);
        afficherResultatTest(2, "AnalyseurLogs::ajouterLigneLogNonTriee (private)", tests.back());

        // Test 3
        analyseurLogs.trierLignesLog();
        bool logsSontOrdonnes1 =
            std::is_sorted(analyseurLogs.logs_.begin(), analyseurLogs.logs_.end(), ComparateurLog());
        tests.push_back(logsSontOrdonnes1);
        afficherResultatTest(3, "AnalyseurLogs::trierLignesLog (private)", tests.back());

        // Test 4
        GestionnaireUtilisateurs gestionnaireUtilisateursFichier;
        gestionnaireUtilisateursFichier.chargerDepuisFichier("utilisateurs.txt");
        GestionnaireJeux gestionnaireJeuxFichier;
        gestionnaireJeuxFichier.chargerDepuisFichier("jeux.txt");
        bool succesChargement =
            analyseurLogs.chargerDepuisFichier("logs.txt", gestionnaireUtilisateursFichier, gestionnaireJeuxFichier);
        bool logsSontOrdonnes2 =
            std::is_sorted(analyseurLogs.logs_.begin(), analyseurLogs.logs_.end(), ComparateurLog());
        tests.push_back(succesChargement && logsSontOrdonnes2 && analyseurLogs.logs_.size() == 10'000);
        afficherResultatTest(4, "AnalyseurLogs::chargerDepuisFichier", tests.back());

        // Test 5
        analyseurLogs = AnalyseurLogs(); // Réinitialiser l'analyseur de logs
        bool creation1 = analyseurLogs.ajouterLigneLog("2018-01-01T00:00:00Z",
                                                       "inconnu@email.com",
                                                       "Nom1",
                                                       1,
                                                       gestionnaireUtilisateurs,
                                                       gestionnaireJeux);
        bool creation2 = analyseurLogs.ajouterLigneLog("2018-01-01T00:00:00Z",
                                                       "prénom.nom.1@email.com",
                                                       "Inconnu",
                                                       2,
                                                       gestionnaireUtilisateurs,
                                                       gestionnaireJeux);
        bool creation3 = analyseurLogs.ajouterLigneLog("2019-01-02T00:00:00Z",
                                                       "prénom.nom.1@email.com",
                                                       "Nom1",
                                                       3,
                                                       gestionnaireUtilisateurs,
                                                       gestionnaireJeux);
        tests.push_back(!creation1 && !creation2 && creation3 && analyseurLogs.logs_.size() == 1 &&
                        analyseurLogs.logs_[0].timestamp == "2019-01-02T00:00:00Z");
        afficherResultatTest(5, "AnalyseurLogs::ajouterLigneLog (overload 1)", tests.back());

        // Test 6
        analyseurLogs = AnalyseurLogs(); // Réinitialiser l'analyseur de logs
        bool ajoutsValides2 = true;
        for (const auto& ligneLog : logsAjoutes)
        {
            ajoutsValides2 = analyseurLogs.ajouterLigneLog(ligneLog) && ajoutsValides2;
        }
        bool ajoutInvalide3 = analyseurLogs.ajouterLigneLog(ligneLogInvalide1);
        bool ajoutInvalide4 = analyseurLogs.ajouterLigneLog(ligneLogInvalide2);
        bool logsSontOrdonnes =
            std::is_sorted(analyseurLogs.logs_.begin(), analyseurLogs.logs_.end(), ComparateurLog());
        int heuresJoueesJeu3 = analyseurLogs.heuresJeux_[pointeursJeux[4]];
        int heuresJoueesJeu4 = analyseurLogs.heuresJeux_[pointeursJeux[5]];
        tests.push_back(ajoutsValides2 && !ajoutInvalide3 && !ajoutInvalide4 && logsSontOrdonnes &&
                        analyseurLogs.logs_.back().timestamp != logsAjoutes.back().timestamp &&
                        heuresJoueesJeu3 == 12 && heuresJoueesJeu4 == 2);
        afficherResultatTest(6, "AnalyseurLogs::ajouterLigneLog (overload 2)", tests.back());

        // Test 7
        Jeu jeuInconnu{"Inconnu", Jeu::Genre::Fps, "Développeur", 1970};
        int heuresJoueesJeu5 = analyseurLogs.getHeuresJoueesPourJeu(&jeuInconnu);
        int heuresJoueesJeu6 = analyseurLogs.getHeuresJoueesPourJeu(nullptr);
        int heuresJoueesJeu7 = analyseurLogs.getHeuresJoueesPourJeu(pointeursJeux[8]);
        tests.push_back(heuresJoueesJeu5 == 0 && heuresJoueesJeu6 == 0 && heuresJoueesJeu7 == 20);
        afficherResultatTest(7, "AnalyseurLogs::getHeuresJoueesPourJeu", tests.back());

        // Test 8
        const Jeu* jeuPlusPopulaire1 = analyseurLogs.getJeuPlusPopulaire();
        AnalyseurLogs temp = analyseurLogs; // Réinitialiser l'analyseur de logs temporairement
        analyseurLogs = AnalyseurLogs();
        const Jeu* jeuPlusPopulaire2 = analyseurLogs.getJeuPlusPopulaire();
        analyseurLogs = temp;
        tests.push_back(jeuPlusPopulaire1 == pointeursJeux[8] && jeuPlusPopulaire2 == nullptr);
        afficherResultatTest(8, "AnalyseurLogs::getJeuPlusPopulaire", tests.back());

        // Test 9
        std::vector<std::pair<const Jeu*, int>> jeuxPlusPopulaires1 = analyseurLogs.getNJeuxPlusPopulaires(3);
        std::vector<std::pair<const Jeu*, int>> jeuxPlusPopulaires1Attendus = {
            std::pair<const Jeu*, int>(pointeursJeux[8], 20),
            std::pair<const Jeu*, int>(pointeursJeux[4], 12),
            std::pair<const Jeu*, int>(pointeursJeux[3], 9),
        };
        std::vector<std::pair<const Jeu*, int>> jeuxPlusPopulaires2 = analyseurLogs.getNJeuxPlusPopulaires(300);
        AnalyseurLogs analyseurLogsVide;
        std::vector<std::pair<const Jeu*, int>> jeuxPlusPopulaires3 = analyseurLogsVide.getNJeuxPlusPopulaires(3);
        tests.push_back(jeuxPlusPopulaires1 == jeuxPlusPopulaires1Attendus &&
                        jeuxPlusPopulaires2.size() == pointeursJeux.size() && jeuxPlusPopulaires3.empty());
        afficherResultatTest(9, "AnalyseurLogs::getNJeuxPlusPopulaires", tests.back());

        // Test 10
        Utilisateur utilisateurInconnu{"inconnu@email.com", "Prénom Nom", 20, Pays::Canada};
        std::size_t nombreSeancesUtilisateur1 = analyseurLogs.getNombreSeancesPourUtilisateur(&utilisateurInconnu);
        std::size_t nombreSeancesUtilisateur2 = analyseurLogs.getNombreSeancesPourUtilisateur(nullptr);
        std::size_t nombreSeancesUtilisateur3 = analyseurLogs.getNombreSeancesPourUtilisateur(pointeursUtilisateurs[1]);
        tests.push_back(nombreSeancesUtilisateur1 == 0 && nombreSeancesUtilisateur2 == 0 &&
                        nombreSeancesUtilisateur3 == 5);
        afficherResultatTest(10, "AnalyseurLogs::getNombreSeancesPourUtilisateur", tests.back());

        // Test 11
        std::vector<const Jeu*> jeuxJoues1 = analyseurLogs.getJeuxJouesParUtilisateur(&utilisateurInconnu);
        std::vector<const Jeu*> jeuxJoues2 = analyseurLogs.getJeuxJouesParUtilisateur(nullptr);
        std::vector<const Jeu*> jeuxJoues3 = analyseurLogs.getJeuxJouesParUtilisateur(pointeursUtilisateurs[3]);
        std::vector<const Jeu*> jeuxJoues3Attendus = {
            pointeursJeux[4],
            pointeursJeux[8],
            pointeursJeux[5],
            pointeursJeux[6],
            pointeursJeux[7],
        };
        std::sort(jeuxJoues3.begin(), jeuxJoues3.end());
        std::sort(jeuxJoues3Attendus.begin(), jeuxJoues3Attendus.end());
        std::vector<const Jeu*> jeuxJoues4 = analyseurLogsVide.getJeuxJouesParUtilisateur(pointeursUtilisateurs[3]);
        tests.push_back(jeuxJoues1.empty() && jeuxJoues2.empty() && jeuxJoues3 == jeuxJoues3Attendus &&
                        jeuxJoues4.empty());
        afficherResultatTest(11, "AnalyseurLogs::getJeuxJouesParUtilisateur", tests.back());

        static constexpr int nombreTestsInsertion = 6;
        int nombreTestsReussisInsertion =
            static_cast<int>(std::count(tests.begin(), tests.begin() + nombreTestsInsertion, true));
        int nombreTestsReussisAlgorithmes =
            static_cast<int>(std::count(tests.begin() + nombreTestsInsertion, tests.end(), true));

        double pointsTestsInsertion = static_cast<double>(nombreTestsReussisInsertion) /
                                      static_cast<double>(nombreTestsInsertion) * maxPointsSection / 3;
        double pointsTestsAlgorithmes = static_cast<double>(nombreTestsReussisAlgorithmes) /
                                        static_cast<double>(tests.size() - nombreTestsInsertion) * maxPointsSection *
                                        2 / 3;
        double totalPointsSection = pointsTestsInsertion + pointsTestsAlgorithmes;
#else
        std::cout << "[Tests désactivés]\n";
        double totalPointsSection = 0.0;
#endif
        afficherFooterTest(totalPointsSection, maxPointsSection);
        return totalPointsSection;
    }
} // namespace Tests
