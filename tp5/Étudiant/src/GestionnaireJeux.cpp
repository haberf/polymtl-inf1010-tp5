/// Gestionnaire de jeux.
/// \author Misha Krieger-Raynauld
/// \date 2020-10-29

#include "GestionnaireJeux.h"
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include "Foncteurs.h"
#include "RawPointerBackInserter.h"


/// Constructeur par copie.
/// \param other    Le gestionnaire de jeux à partir duquel copier la classe.
GestionnaireJeux::GestionnaireJeux(const GestionnaireJeux& other)
{
    jeux_.reserve(other.jeux_.size());
    filtreNomJeux_.reserve(other.filtreNomJeux_.size());
    filtreGenreJeux_.reserve(other.filtreGenreJeux_.size());
    filtreDeveloppeurJeux_.reserve(other.filtreDeveloppeurJeux_.size());

    for (const auto& jeu : other.jeux_)
    {
       
        ajouterJeu(std::make_unique<Jeu>(*jeu));
    }
}

/// Opérateur d'assignation par copie utilisant le copy-and-swap idiom.
/// \param other    Le gestionnaire de jeux à partir duquel copier la classe.
/// \return         Référence à l'objet actuel.
GestionnaireJeux& GestionnaireJeux::operator=(GestionnaireJeux other)
{
    std::swap(jeux_, other.jeux_);
    std::swap(filtreNomJeux_, other.filtreNomJeux_);
    std::swap(filtreGenreJeux_, other.filtreGenreJeux_);
    std::swap(filtreDeveloppeurJeux_, other.filtreDeveloppeurJeux_);
    return *this;
}

/// Affiche les informations des jeux gérés par le gestionnaire de jeux à la sortie du stream donné.
/// \param outputStream     Le stream auquel écrire les informations des jeux.
/// \param gestionnaireJeux Le gestionnaire de jeux à afficher au stream.
/// \return                 Une référence au stream.
std::ostream& operator<<(std::ostream& outputStream, const GestionnaireJeux& gestionnaireJeux)
{
    
    outputStream << "Le gestionnaire de jeux contient " /* << gestionnaireJeux.getNombreJeux() */ << " jeux.\n"
        << "Affichage par catégories:\n";

   
    for (const auto& [key, value] : gestionnaireJeux.filtreGenreJeux_)
    {
        Jeu::Genre genre = key;
        std::vector<const Jeu*> listeJeux = value;
        outputStream << "Genre: " << getGenreString(genre) << " (" << listeJeux.size() << " jeux):\n";
        for (auto& element : listeJeux)
        {
            outputStream << '\t' << *element << '\n';
        }
    }
    return outputStream;
}

/// Ajoute les jeux à partir d'un fichier de description des jeux.
/// \param nomFichier   Le fichier à partir duquel lire les informations des jeux.
/// \return             True si tout le chargement s'est effectué avec succès, false sinon.
bool GestionnaireJeux::chargerDepuisFichier(const std::string& nomFichier)
{
    std::ifstream fichier(nomFichier);
    if (!fichier)
    {
        std::cerr << "Erreur GestionnaireJeux: le fichier " << nomFichier << " n'a pas pu être ouvert\n";
        return false;
    }

    jeux_.clear();
    filtreNomJeux_.clear();
    filtreGenreJeux_.clear();
    filtreDeveloppeurJeux_.clear();

    bool succesParsing = true;

    std::string ligne;
    while (std::getline(fichier, ligne))
    {
        std::istringstream stream(ligne);

        std::string nom;
        int genre;
        std::string developpeur;
        int annee;

        if (stream >> std::quoted(nom) >> genre >> std::quoted(developpeur) >> annee)
        {
      
            ajouterJeu(std::make_unique<Jeu>(Jeu{ nom, static_cast<Jeu::Genre>(genre), developpeur, annee }));
        }
        else
        {
            std::cerr << "Erreur GestionnaireJeux: la ligne " << ligne << " n'a pas pu être interprétée correctement\n";
            succesParsing = false;
        }
    }
    return succesParsing;
}

bool GestionnaireJeux::ajouterJeu(std::unique_ptr<Jeu> jeu)
{
    if (getJeuParNom(jeu->nom) != nullptr)
        return false;
    jeux_.push_back(std::move(std::make_unique<Jeu>(jeu)));
    filtreNomJeux_.emplace(jeu->nom, jeux_.back().get());
    filtreGenreJeux_[jeu->genre].push_back(jeux_.back().get());
    filtreDeveloppeurJeux_[jeu->developpeur].push_back(jeux_.back().get());

    return true;
}

bool GestionnaireJeux::supprimerJeu(const std::string& nomJeu)
{
    std::vector<std::unique_ptr<Jeu>>::iterator trouve = find_if(jeux_.begin(), jeux_.end(), [&nomJeu](std::unique_ptr<Jeu>& jeu) {return jeu->nom == nomJeu; });
    if (trouve == jeux_.end())
        return false;
    Jeu jeu = *(trouve->get());
    filtreNomJeux_.erase(nomJeu);
    std::vector<const Jeu*>& vecteurDeveloppeurs = filtreDeveloppeurJeux_[jeu.developpeur];
    std::vector<const Jeu*>& vecteurGenre = filtreGenreJeux_[jeu.genre];

    vecteurDeveloppeurs.erase(std::remove(vecteurDeveloppeurs.begin(), vecteurDeveloppeurs.end(), trouve->get()), vecteurDeveloppeurs.end());
    vecteurGenre.erase(std::remove(vecteurGenre.begin(), vecteurGenre.end(), trouve->get()), vecteurGenre.end());

    jeux_.erase(trouve);

    return true;
}

std::size_t GestionnaireJeux::getNombreJeux() const
{
    return std::size_t();
}

const Jeu* GestionnaireJeux::getJeuParNom(const std::string& nom) const
{

    auto jeu = filtreNomJeux_.find(nom);
    if (jeu == filtreNomJeux_.end())
        return nullptr;
    return jeu->second;
}

std::vector<const Jeu*> GestionnaireJeux::getJeuxParGenre(Jeu::Genre genre) const
{

    auto jeu = filtreGenreJeux_.find(genre);
    if (jeu == filtreGenreJeux_.end())
        return std::vector<const Jeu*>();
    return filtreGenreJeux_.at(genre);
}

std::vector<const Jeu*> GestionnaireJeux::getJeuxParDeveloppeur(const std::string& developpeur) const
{
    auto it = filtreDeveloppeurJeux_.find(developpeur);
    if (it == filtreDeveloppeurJeux_.end())
        return std::vector<const Jeu*>();
    return filtreDeveloppeurJeux_.at(developpeur);
}

std::vector<const Jeu*> GestionnaireJeux::getJeuxEntreAnnees(int anneeDebut, int anneeFin) const
{
    std::vector<const Jeu*> JeuxEntreAnnees;
    copy_if(jeux_.begin(), jeux_.end(), RawPointerBackInserter(JeuxEntreAnnees), EstDansIntervalleDatesJeu(anneeDebut, anneeFin));
    return JeuxEntreAnnees;
}
