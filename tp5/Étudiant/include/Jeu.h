/// Struct pour les jeux.
/// \author Misha Krieger-Raynauld
/// \date 2020-10-29

#ifndef JEU_H
#define JEU_H

#include <iostream>
#include <string>

/// Struct contenant les caractéristiques pour un jeu.
struct Jeu
{
    /// Enum pour le genre (catégorie) d'un jeu.
    enum class Genre
    {
        Fps,
        Platformer,
        Course,
        Action,
        Aventure,
        Rpg,
        Mmo,
        Simulation,
        Sport,
        Autre,
    };

    std::string nom;
    Genre genre;
    std::string developpeur;
    int annee;
};

std::string getGenreString(Jeu::Genre genre);
std::ostream& operator<<(std::ostream& outputStream, const Jeu& jeu);

#endif // JEU_H
