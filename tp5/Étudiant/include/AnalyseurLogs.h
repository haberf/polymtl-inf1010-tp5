/// Analyseur de statistiques grâce aux logs.
/// \author Misha Krieger-Raynauld
/// \date 2020-10-29

#ifndef ANALYSEURLOGS_H
#define ANALYSEURLOGS_H

#include <string>
#include <vector>
#include "GestionnaireJeux.h"
#include "GestionnaireUtilisateurs.h"
#include "LigneLog.h"
#include "Tests.h"

/// Classe contenant la liste des entrées du log pour en analyser les tendances pertinentes.
class AnalyseurLogs
{
public:
    // Opérations d'ajout de lignes de log
    bool chargerDepuisFichier(const std::string& nomFichier, GestionnaireUtilisateurs& gestionnaireUtilisateurs,
                              GestionnaireJeux& gestionnaireJeux);
    bool ajouterLigneLog(const std::string& timestamp, const std::string& idUtilisateur, const std::string& nomJeu,
                         int heures, GestionnaireUtilisateurs& gestionnaireUtilisateurs,
                         GestionnaireJeux& gestionnaireJeux);
    bool ajouterLigneLog(const LigneLog& ligneLog);

    // Statistiques
    int getHeuresJoueesPourJeu(const Jeu* jeu) const;
    const Jeu* getJeuPlusPopulaire() const;
    std::vector<std::pair<const Jeu*, int>> getNJeuxPlusPopulaires(std::size_t nombre) const;
    int getNombreSeancesPourUtilisateur(const Utilisateur* utilisateur) const;
    std::vector<const Jeu*> getJeuxJouesParUtilisateur(const Utilisateur* utilisateur) const;

private:
    // Fonctions privées pour l'ajout de lignes de log
    LigneLog creerLigneLog(const std::string& timestamp, const std::string& idUtilisateur, const std::string& nomJeu,
                           int heures, GestionnaireUtilisateurs& gestionnaireUtilisateurs,
                           GestionnaireJeux& gestionnaireJeux);
    bool ajouterLigneLogNonTriee(const LigneLog& ligneLog);
    void trierLignesLog();

    std::vector<LigneLog> logs_;
    std::unordered_map<const Jeu*, int> heuresJeux_;

    friend double Tests::testAnalyseurLogs(); // Pour les tests
};

#endif // ANALYSEURLOGS_H
