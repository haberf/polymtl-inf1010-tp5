#include<memory>
#include"Jeu.h"
#include"LigneLog.h"


class EstDansIntervalleDatesJeu
{
public:
	EstDansIntervalleDatesJeu(const int inferieur, const int superieur) :
		inferieur_(inferieur),
		superieur_(superieur)
	{}
	bool operator()(const std::unique_ptr<Jeu>& comparable)
	{
		return comparable->annee >= inferieur_ && comparable->annee <= superieur_;
	}
private:
	int inferieur_;
	int superieur_;
};

class ComparateurLog
{
public:
	ComparateurLog() {};
	bool operator()(const LigneLog L1, const LigneLog L2)
	{
		return L1.timestamp < L2.timestamp;
	};
};
template<typename T1, typename T2>
class ComparateurSecondElementPaire
{
public:
	ComparateurSecondElementPaire() {}

	bool operator()(const std::pair<T1, T2>& P1, const std::pair<T1, T2>& P2)
	{
		return std::get<1>(P1) < std::get<1>(P2);
	}

};