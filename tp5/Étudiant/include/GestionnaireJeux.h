/// Gestionnaire de jeux.
/// \author Misha Krieger-Raynauld
/// \date 2020-10-29

#ifndef GESTIONNAIREJEUX_H
#define GESTIONNAIREJEUX_H

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>
#include "Jeu.h"
#include "Tests.h"

/// Classe qui gère les informations de tous les jeux et qui conserve des filtres pour les rechercher rapidement.
class GestionnaireJeux
{
public:
    // Fonctions membres spéciales
    GestionnaireJeux() = default;
    GestionnaireJeux(const GestionnaireJeux& other);
    GestionnaireJeux(GestionnaireJeux&&) = default;
    GestionnaireJeux& operator=(GestionnaireJeux other);

    // Surcharges d'opérateurs
    friend std::ostream& operator<<(std::ostream& outputStream, const GestionnaireJeux& gestionnaireJeux);

    // Opérations d'ajout et de suppression
    bool chargerDepuisFichier(const std::string& nomFichier);
    bool ajouterJeu(std::unique_ptr<Jeu> jeu);
    bool supprimerJeu(const std::string& nomJeu);

    // Getters
    std::size_t getNombreJeux() const;
    const Jeu* getJeuParNom(const std::string& nom) const;
    std::vector<const Jeu*> getJeuxParGenre(Jeu::Genre genre) const;
    std::vector<const Jeu*> getJeuxParDeveloppeur(const std::string& developpeur) const;
    std::vector<const Jeu*> getJeuxEntreAnnees(int anneeDebut, int anneeFin) const;

private:
    std::vector<std::unique_ptr<Jeu>> jeux_; // Vecteur de pointeurs pour ne pas que les éléments des filtres
                                             // deviennent invalidés lors d'un resize du vecteur

    std::unordered_map<std::string, const Jeu*> filtreNomJeux_;
    std::unordered_map<Jeu::Genre, std::vector<const Jeu*>> filtreGenreJeux_;
    std::unordered_map<std::string, std::vector<const Jeu*>> filtreDeveloppeurJeux_;

    friend double Tests::testGestionnaireJeux(); // Pour les tests
};

#endif // GESTIONNAIREJEUX_H
