/// Struct pour les lignes de log.
/// \author Misha Krieger-Raynauld
/// \date 2020-10-29

#ifndef LIGNELOG_H
#define LIGNELOG_H

#include <string>
#include "Jeu.h"
#include "Utilisateur.h"

/// Struct contenant les informations traduites d'une ligne du log.
struct LigneLog
{
    std::string timestamp;
    const Utilisateur* utilisateur;
    const Jeu* jeu;
    int heures;
};

#endif // LIGNELOG_H
